const config = {
	mode: 'jit',
	purge: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				links: '#479DFF'
			}
		}
	},
	plugins: []
};

module.exports = config;
